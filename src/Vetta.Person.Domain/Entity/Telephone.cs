﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vetta.Person.Domain.Entity
{
    public class Telephone
    {
        public Guid Id { get; set; }
        public Guid PersonId { get; set; }
        public string PhoneNumber { get; set; }
    }
}
