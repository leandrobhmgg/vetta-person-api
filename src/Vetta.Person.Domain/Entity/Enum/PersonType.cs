﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vetta.Person.Domain.Entity.Enum
{
    public enum PersonType
    {
        Individual = 1,
        Corporate = 2
    }
}
