﻿namespace Vetta.Person.Domain.Entity.Enum
{
    public enum ClassificationType
    {
        Active = 1,
        Inactive = 2,
        Preferential = 2
    }
}
