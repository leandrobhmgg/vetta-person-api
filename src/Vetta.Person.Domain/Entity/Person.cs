﻿using System;
using System.Collections.Generic;
using Vetta.Person.Domain.Entity.Enum;

namespace Vetta.Person.Domain.Entity
{
    public class Person
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ZipCode { get; set; }
        public string Email { get; set; }
        public string Document { get; set; }
        public ClassificationType Classification { get; set; }
        public ICollection<Telephone> Telephones { get; set; }
        public PersonType Type { get; set; }
        public Person(PersonType type)
        {
            Type = type;
        }

        public Person()
        {

        }
    }
}