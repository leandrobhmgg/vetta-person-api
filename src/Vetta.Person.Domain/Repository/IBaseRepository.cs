﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Vetta.Person.Domain.Repository
{
    public interface IBaseRepository<TEntity, TEntityId>
    {
        Task<TEntity> GetByIdAsync(TEntityId id, params string[] expands);
        Task<IEnumerable<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> expression, params string[] expands);
        Task AddAsync(TEntity entity);
        Task UpdateAsync(TEntity entity);
        Task<TEntity> RemoveAsync(TEntityId id);
    }
}
