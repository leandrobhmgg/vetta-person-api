﻿using System;
namespace Vetta.Person.Domain.Repository
{
    public interface IPersonRepository : IBaseRepository<Vetta.Person.Domain.Entity.Person, Guid>
    {

    }
}
