﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Vetta.Person.Domain.Repository;

namespace Vetta.Person.Domain.Service
{

    public class BaseService<TEntity, TEntityId> : IBaseService<TEntity, TEntityId>
   where TEntity : class
    {
        private readonly IBaseRepository<TEntity, TEntityId> _repository;
        public BaseService(IBaseRepository<TEntity, TEntityId> repository)
        {
            _repository = repository;
        }


        public virtual Task AddAsync(TEntity entity)
        {
            return _repository.AddAsync(entity);
        }

        public Task<IEnumerable<TEntity>> GetAllAsync
        (
            Expression<Func<TEntity, bool>> expression,
             params string[] expands
        )
        {
            return _repository.GetAllAsync(expression, expands);
        }

        public Task<TEntity> GetByIdAsync(TEntityId id, params string[] expands)
        {
            return _repository.GetByIdAsync(id, expands);
        }

        public Task UpdateAsync(TEntity entity)
        {
            return _repository.UpdateAsync(entity);
        }
        public Task<TEntity> RemoveAsync(TEntityId id)
        {
            return _repository.RemoveAsync(id);
        }
    }

}
