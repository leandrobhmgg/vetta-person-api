﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vetta.Person.Domain.Service
{
    public interface IPersonService : IBaseService<Vetta.Person.Domain.Entity.Person, Guid>
    {
    }

}
