using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Vetta.Person.Application.Extensions;
using Vetta.Person.Infrastructure.Context;
using Vetta.Person.Infrastructure.Extensions;
using Vetta.Person.Web.Models;

namespace Vetta.Person.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            services.AddApplicationExtensions();

            services.AddInfrastructureExtensions(Configuration);

            AutoMapperExtensions(services);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Person/Error");

                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Person}/{action=Index}/{id?}");
            });

            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<PersonContext>();
                context.Database.EnsureCreated();
            }

        }

        public void AutoMapperExtensions(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(Startup));

            var config = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.CreateMap<PersonViewModel, Domain.Entity.Person>().ReverseMap();
                cfg.CreateMap<PersonTypeViewModel, Domain.Entity.Enum.PersonType>().ReverseMap();
                cfg.CreateMap<TelephoneViewModel, Domain.Entity.Telephone>().ReverseMap();
            });
            IMapper mapper = config.CreateMapper();
            services.AddSingleton(mapper);

        }
    }
}