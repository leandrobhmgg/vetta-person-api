﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Vetta.Person.Domain.Core;
using Vetta.Person.Domain.Service;
using Vetta.Person.Web.Models;

namespace Vetta.Person.Web.Controllers
{
    public class PersonController : Controller
    {
        private readonly IPersonService _personService;
        private readonly IMapper _mapper;
        public PersonController(IPersonService personService, IMapper mapper)
        {
            _personService = personService;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index(string search)
        {
            var viewModel = _mapper.Map<IEnumerable<PersonViewModel>>(
                                            await _personService.GetAllAsync(GetFilter(search),
                                            nameof(Domain.Entity.Person.Telephones))
                                      );
            return View(viewModel);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View(new PersonViewModel());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PersonViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                await _personService.AddAsync(_mapper.Map<Domain.Entity.Person>(viewModel));

                return RedirectToAction("Index");
            }
            return View(viewModel);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            PersonViewModel person = _mapper.Map<PersonViewModel>(
                await _personService.GetByIdAsync(id.Value, nameof(Domain.Entity.Person.Telephones))
                );

            if (person == null)
            {
                return NotFound();
            }

            return View(person);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, PersonViewModel person)
        {
            if (id != person.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                await _personService.UpdateAsync(_mapper.Map<Domain.Entity.Person>(person));

                return RedirectToAction("Index");
            }
            return View(person);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            Vetta.Person.Domain.Entity.Person person = await _personService.GetByIdAsync(id.Value);
            if (person == null)
            {
                return NotFound();
            }
            return View(person);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid? id)
        {
            await _personService.RemoveAsync(id.Value);
            return RedirectToAction("Index");
        }

        private Expression<Func<Domain.Entity.Person, bool>> GetFilter(string search)
        {
            var filter = PredicateBuilder.True<Domain.Entity.Person>();

            if (!String.IsNullOrEmpty(search))
                filter = filter.And(x => x.Name.Contains(search));

            return filter;
        }

    }
}