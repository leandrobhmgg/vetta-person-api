﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Vetta.Person.Web.Models
{
    public class PersonViewModel
    {
        public PersonViewModel()
        {
            Telephones = new List<TelephoneViewModel>();
        }

        public Guid Id { get; set; }

        [Display(Name = "Nome / Razao social")]
        [StringLength(100, ErrorMessage = "Tamanho máximo de 100 caracteres")]
        [Required(ErrorMessage = "Nome / Razao social é obrigatório")]
        public string Name { get; set; }

        [Display(Name = "CPF / CNPJ")]
        public string Document { get; set; }

        [Display(Name = "CEP")]
        public string ZipCode { get; set; }

        [Display(Name = "E-mail")]
        [Required(ErrorMessage = "e-mail é obrigatório")]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail em formato inválido.")]
        public string Email { get; set; }
        [Display(Name = "Classificação")]
        [Required(ErrorMessage = "Informe o tipo de classificação")]
        public ClassificationTypeViewModel Classification { get; set; }

        [Display(Name = "Telefones")]
        public List<TelephoneViewModel> Telephones { get; set; }

        [Display(Name = "Tipo de pessoa")]
        [Required(ErrorMessage = "Informe o tipo de pessoa: Física/Jurídica")]
        public PersonTypeViewModel Type { get; set; }
    }
}
