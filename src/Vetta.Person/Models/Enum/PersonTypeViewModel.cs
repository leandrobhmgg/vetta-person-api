﻿using System.ComponentModel.DataAnnotations;

namespace Vetta.Person.Web.Models
{
    public enum PersonTypeViewModel
    {
        [Display(Name = "Física")]
        Individual = 1,
        [Display(Name = "Jurídica")]
        Corporate = 2
    }
}
