﻿using System.ComponentModel.DataAnnotations;

namespace Vetta.Person.Web.Models
{
    public enum ClassificationTypeViewModel
    {
        [Display(Name = "Ativo")]
        Active = 1,
        [Display(Name = "Inativo")]
        Inactive = 2,
        [Display(Name = "Preferencial")]
        Preferential = 2
    }
}
