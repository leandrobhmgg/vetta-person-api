﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Vetta.Person.Web.Models
{
    public class TelephoneViewModel
    {
        public Guid Id { get; set; }
        public Guid PersonId { get; set; }
        
        [Display(Name = "Numero de Telefone")]
        public string PhoneNumber { get; set; }
    }
}