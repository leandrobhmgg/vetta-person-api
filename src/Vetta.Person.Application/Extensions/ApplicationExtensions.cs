﻿using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using Vetta.Person.Application.Person;
using Vetta.Person.Application.Validators;
using Vetta.Person.Domain.Service;

namespace Vetta.Person.Application.Extensions
{

    public static class ApplicationExtensions
    {
        public static void AddApplicationExtensions(this IServiceCollection services)
        {
            services.AddScoped<IPersonService, PersonService>();
            services.AddScoped<IValidator<Domain.Entity.Person>, PersonValidator>();
        }
    }
}
