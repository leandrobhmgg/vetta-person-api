﻿using FluentValidation;

namespace Vetta.Person.Application.Validators
{
    public class PersonValidator : AbstractValidator<Domain.Entity.Person>
    {
        public PersonValidator()
        {
            RuleFor(x => x.Name).NotEmpty().MaximumLength(100).WithMessage("campo nao pode conter mais de 100 caracteres");
            RuleFor(x => x.Email).NotEmpty().EmailAddress().WithMessage("email invalido");
            RuleFor(x => x.ZipCode).MaximumLength(30).WithMessage("tamanho maior que o permitido");
            RuleFor(x => x.Classification).NotNull();
            RuleFor(x => x.Type).NotNull();
        }
    }
}