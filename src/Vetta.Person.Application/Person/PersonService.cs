﻿using FluentValidation;
using System;
using System.Threading.Tasks;
using Vetta.Person.Domain.Repository;
using Vetta.Person.Domain.Service;

namespace Vetta.Person.Application.Person
{

    public class PersonService : BaseService<Domain.Entity.Person, Guid>, IPersonService
    {
        private readonly IValidator<Domain.Entity.Person> _validator;

        public PersonService(IBaseRepository<Domain.Entity.Person, Guid> repository, IValidator<Domain.Entity.Person> validator)
            : base(repository)
        {
            _validator = validator;
        }

        public override Task AddAsync(Domain.Entity.Person entity)
        {
            var result = _validator.Validate(entity);

            if (result.Errors.Count > 0)
                throw new ValidationException(result.Errors);

            return base.AddAsync(entity);
        }

    }
}
