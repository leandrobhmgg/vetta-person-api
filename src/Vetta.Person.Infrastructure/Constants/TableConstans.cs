﻿namespace Vetta.Person.Infrastructure.Constants
{

    public static class TableConstans
    {
        public const string TablePerson = "person";

        public const string ColumnPersonId = "personId";
        public const string ColumnName = "name";
        public const string ColumnDocument = "document";
        public const string ColumnZipCode = "zipCode";
        public const string ColumnEmail = "email";
        public const string ColumnClassification = "classification";
        public const string ColumnTelephone = "telephone";
        public const string ColumnType = "type";

    }

}
