﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Vetta.Person.Domain.Repository;
using Vetta.Person.Infrastructure.Extensions;

namespace Vetta.Person.Infrastructure.Repository
{
    public class BaseRepository<TEntity, TEntityId, TDbContext> : IBaseRepository<TEntity, TEntityId>
      where TEntity : class
      where TDbContext : DbContext
    {
        private readonly TDbContext _context;
        public BaseRepository(TDbContext context)
        {
            _context = context;
        }

        public Task AddAsync(TEntity entity)
        {
            _context.Set<TEntity>().AddAsync(entity);
            return _context.SaveChangesAsync();
        }
        public async Task<IEnumerable<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> expression, params string[] expands)
        {
            return await _context.Set<TEntity>().Expands(expands).Where(expression).ToListAsync();
        }
        public async Task<TEntity> GetByIdAsync(TEntityId id, params string[] expands)
        {
            var item = await _context.Set<TEntity>().FindAsync(id);
            return item != default ? _context.Set<TEntity>().Expands(expands).First(x => x == item) : default;
        }
        public async Task<TEntity> RemoveAsync(TEntityId id)
        {
            var entity = await GetByIdAsync(id);
            _context.Set<TEntity>().Remove(entity);
            await _context.SaveChangesAsync();
            return entity;
        }
        public Task UpdateAsync(TEntity entity)
        {
            _context.Set<TEntity>().Remove(entity);
            _context.SaveChanges();

            _context.Set<TEntity>().AddAsync(entity);
            return _context.SaveChangesAsync();
        }
    }
}
