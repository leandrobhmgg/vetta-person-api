﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Vetta.Person.Infrastructure.Constants;

namespace Vetta.Person.Infrastructure.Maps
{

    public class PersonMap : IEntityTypeConfiguration<Domain.Entity.Person>
    {
        public void Configure(EntityTypeBuilder<Domain.Entity.Person> builder)
        {
            builder.ToTable(TableConstans.TablePerson);

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Id)
            .HasColumnName(TableConstans.ColumnPersonId);

            builder.Property(p => p.Name)
            .HasColumnName(TableConstans.ColumnName)
            .HasMaxLength(100)
            .IsRequired();

            builder.Property(p => p.Document)
            .HasColumnName(TableConstans.ColumnDocument);

            builder.Property(p => p.ZipCode)
            .HasColumnName(TableConstans.ColumnZipCode)
            .HasMaxLength(30);

            builder.Property(p => p.Email)
            .HasColumnName(TableConstans.ColumnEmail);


            builder.Property(p => p.Classification)
            .HasColumnName(TableConstans.ColumnClassification)
            .IsRequired();

            builder.Property(p => p.Type)
            .HasColumnName(TableConstans.ColumnType)
            .IsRequired();

            //builder.Property(p => p.Telephones)
            //.HasColumnName(TableConstans.ColumnTelephone)
            //.IsRequired();
        }
    }
}

