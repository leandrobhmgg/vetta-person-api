﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using Vetta.Person.Domain.Repository;
using Vetta.Person.Infrastructure.Context;
using Vetta.Person.Infrastructure.Repository;

namespace Vetta.Person.Infrastructure.Extensions
{
    public static class InfrastructureExtensions
    {
        public static void AddInfrastructureExtensions(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IBaseRepository<Domain.Entity.Person, Guid>, BaseRepository<Domain.Entity.Person, Guid, PersonContext>>();

            services.AddDbContext<PersonContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("SqlServer"));
            });

        }
    }
}
