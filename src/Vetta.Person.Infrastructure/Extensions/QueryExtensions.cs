﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Vetta.Person.Infrastructure.Extensions
{
    public static class QueryExtensions
    {
        public static IQueryable<TEntity> Expands<TEntity>(this IQueryable<TEntity> query, params string[] expands)
       where TEntity : class
        {
            foreach (string include in expands)
                query = query.Include(include);

            return query;
        }
    }

}
