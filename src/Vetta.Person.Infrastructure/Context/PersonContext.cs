﻿using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Vetta.Person.Infrastructure.Context
{
    public class PersonContext : DbContext
    {
        public PersonContext(DbContextOptions<PersonContext> options)
        : base(options)
        {
        }


        public DbSet<Domain.Entity.Person> Person { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}